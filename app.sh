#!/usr/bin/env bash
while ! python manage.py migrate; do
    sleep 5
    echo '--- waiting for pg'
done
python manage.py runserver 0.0.0.0:8000
