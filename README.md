# Crispy Succotash

## CI test strategy

- Build stage
  - This stage builds docker image and stores it in GitLab registry
  - We're using this docker image in next stage

- Test stage
  - Django tests
    - Runs unit tests
  - Cypress tests
    - Installs docker and docker-compose to start the application
    - Remote docker host provided by GitLab service `docker:stable-dind` is being used as docker daemon
    - `docker-compose up` to start the application (Docker images are not built here to save some time)
    - Cypress tests are being run in following order:
        - user sign-up
        - user login
        - create new feed for `http://www.nu.nl/rss/Algemeen`
        - bookmark to a feed
- Report stage
  - Uses `mochawesome` module to create a test report as an HTML page for Cypress tests
  - HTML report is being served using GitLab pages.
  - Report can be accessed [here](https://snehalbhuktar.gitlab.io/crispy-succotash/) 