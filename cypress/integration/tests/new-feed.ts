import * as pages from '../../pages/index';
import * as data from '../../fixtures/data.json';

describe('When user is on new feed page', () => {
    const redirectPage = new pages.RedirectPage;
    const loginPage = new pages.LoginPage;
    const feedsPage = new pages.FeedPage;
    const newFeedPage = new pages.NewFeedPage;

    beforeEach('Before every test', () => {
        redirectPage.goToLoginPage();
        loginPage.login(data.username, data.password);
    });

    it('Then user is able to feed new url successfully', () => {
        feedsPage.goToNewFeeds();
        newFeedPage.submitNewFeed(data.newFeed);
        // Assertion
    })

    it('Then user is not able to feed same url again', () => {
        feedsPage.goToNewFeeds();
        newFeedPage.submitNewFeed(data.newFeed);
        // Assertion for error message
        cy.get('#error_1_id_feed_url').should('be.visible')
            .should('contain', 'Feed with this Feed URL already exists.');
    })
});