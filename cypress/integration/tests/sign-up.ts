import * as pages from '../../pages/index';

describe('When user is on Signup page', () => {

    const redirectPage = new pages.RedirectPage;
    const signupPage = new pages.SignUpPage;

    beforeEach('Before every test', () => {
        redirectPage.goToSignUpPage();
    });

    it('Then user is able to signup successfully', () => {
        signupPage.signUp('user1_test', 'P@ssword11', 'P@ssword11');
        // Assertion
        cy.url().should('contain', 'feeds');
        cy.get('h1').should('be.visible')
            .should('contain', 'Feeds');
    })

    it('Then user gets an error message if the username already exists', () => {
        signupPage.signUp('user1_test', 'P@ssword11', 'P@ssword11');
        // Assertion for error
        cy.get('#error_1_id_username').should('be.visible')
            .should('contain', 'A user with that username already exists.');
        // Assertion for hint message
        cy.get('#hint_id_username').should('be.visible')
            .should('contain', 'Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.');

    });

    it('Then user gets an error message if passwords does not match', () => {
        signupPage.signUp('user1_test', 'P@ssword11', 'P@ssword12');
        // Assertion for error
        cy.get('#error_1_id_password2').should('be.visible')
            .should('contain', 'The two password fields didn\'t match.');
        // Assertion for the hint message
        cy.get('#hint_id_password2').should('be.visible')
            .should('contain', 'Enter the same password as before, for verification.');
    });
});