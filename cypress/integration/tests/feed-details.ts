import * as pages from '../../pages/index';
import * as data from '../../fixtures/data.json';

describe('When user is on specific feed details page', () => {
    const redirectPage = new pages.RedirectPage;
    const loginPage = new pages.LoginPage;
    const feedsPage = new pages.FeedPage;
    const newFeedPage = new pages.NewFeedPage;
    const feed1Page = new pages.Feed1Page;
    const feed13Page = new pages.Feed13Page;

    beforeEach('Before every test', () => {
        redirectPage.goToLoginPage();
        loginPage.login(data.username, data.password);
    });

    it('Then user is able to bookmark the feed successfully', () => {
        feedsPage.goToFeed1();
        feed1Page.bookmarkFeed1();
        feed1Page.goToBookmarked();
        // Assertion
        cy.get('[href="/feeds/1"]').should('be.visible')
            .should('contain', 'NU - Algemeen');
    });

    it('Then user is able to add comment for the feed', () => {
        feedsPage.goToFeed1();
        feed1Page.goToFeedEntry13();
        feed13Page.addComment('#Hello world');
        // Assertion
        cy.get('#comment-1').should('be.visible');
    });
});