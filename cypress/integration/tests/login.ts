import * as pages from '../../pages/index';
import * as data from '../../fixtures/data.json';

describe('When user is on Login page', () => {

    const redirectPage = new pages.RedirectPage;
    const signupPage = new pages.SignUpPage;
    const loginPage = new pages.LoginPage;
    const feedPage = new pages.FeedPage;

    beforeEach('Before every test', () => {
        redirectPage.goToSignUpPage();
        signupPage.goToLoginPage();
    })

    it('Then user is able to login and logout successfully', () => {
        loginPage.login(data.username, data.password);
        // Assertion
        cy.url().should('contain', 'feeds');
        cy.get('h1').should('be.visible')
            .should('contain', 'Feeds');
        // Logout
        feedPage.logout();
    });

    it('Then user is not able to login with invalid username or password', () => {
        loginPage.login('user', 'P@ssword11');
        // Assertion
        cy.get('.alert-block').should('be.visible')
            .should('contain', 'Please enter a correct username and password. Note that both fields may be case-sensitive.');
    });
});