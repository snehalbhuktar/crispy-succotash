export default class Feed13Page{

    private commentBox = '.CodeMirror-line';
    private submitButton = '#submit-id-submit';
    
    addComment(comment: string): void{
        // cy.get(this.commentBox).type(comment);
        cy.get(this.commentBox).type(comment);
        cy.get(this.submitButton).click();
    }
}