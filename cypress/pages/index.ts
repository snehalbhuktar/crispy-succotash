import RedirectPage from "./redirect-page";
import SignUpPage from "./signup-page";
import LoginPage from "./login-page";
import FeedPage from "./feeds-page";
import NewFeedPage from "./new-feed-page";
import Feed1Page from "./feed1-page";
import Feed13Page from "./feed-13-page";

export{
    RedirectPage,
    SignUpPage,
    LoginPage,
    FeedPage,
    NewFeedPage,
    Feed1Page,
    Feed13Page
}