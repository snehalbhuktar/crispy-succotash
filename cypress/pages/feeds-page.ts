export default class FeedPage{
    private newFeedButton = '[href="/feeds/new/"]';
    private feed1Url = '[href="/feeds/1"]';
    private bookmarkedTabLocator = '[href="/feeds/bookmarked/"]';
    private logoutLink = '[href="/accounts/logout/"]';

    goToNewFeeds(): void{
        cy.get(this.newFeedButton).click();
    }
    
    goToFeed1(): void{
        cy.get(this.feed1Url).click();
    }
    goToBookmarked(): void{
        cy.get(this.bookmarkedTabLocator).click();
    }
    logout(): void{
        cy.get(this.logoutLink).click();
    }

}