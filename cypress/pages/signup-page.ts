export default class SignUpPage{

    private userNameInputBox = '#id_username';
    private passwordInputBox = '#id_password1';
    private confirmPasswordInputBox = '#id_password2';
    private submitButton = '#submit-id-submit';
    private loginLink = '[href="/accounts/login/"]';

    signUp(username: string, password:string, confirmPassword: string): void{
        cy.get(this.userNameInputBox).type(username);
        cy.get(this.passwordInputBox).type(password);
        cy.get(this.confirmPasswordInputBox).type(confirmPassword);
        cy.get(this.submitButton).click();
    }
    goToLoginPage(): void{
        cy.get(this.loginLink).click();
    }

}