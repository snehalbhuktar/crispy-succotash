export default class LoginPage{

    private usernameInputBox = '#id_username';
    private passwordInputBox = '#id_password';
    private loginButton = '[type="submit"]';

    login(username: string, password: string): void{
        cy.get(this.usernameInputBox).type(username);
        cy.get(this.passwordInputBox).type(password);
        cy.get(this.loginButton).click();
    }
}