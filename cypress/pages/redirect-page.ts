export default class RedirectPage{
    
    private signUpButton = '[href="/accounts/register/"]';
    private loginLink = '[href="/accounts/login/"]';

    goToSignUpPage(): void{
        cy.visit('/');
        cy.get(this.signUpButton).click();
    }

    goToLoginPage(): void{
        cy.visit('/');
        cy.get(this.loginLink).click();
    }
}