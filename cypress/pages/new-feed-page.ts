export default class NewFeedPage{

    private feedUrlInputBox = '#id_feed_url';
    private submitButton = '#submit-id-submit';

    submitNewFeed(feedUrl: string): void{
        cy.get(this.feedUrlInputBox).type(feedUrl);
        cy.get(this.submitButton).click();
    }
}