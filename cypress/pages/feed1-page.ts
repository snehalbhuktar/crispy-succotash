export default class Feed1Page{

    private bookmarkIcon = '[href="/feeds/1/toggle-bookmark/"]';
    private bookmarkedTabLocator = '[href="/feeds/bookmarked/"]';
    private feedEntry288Locator = '[href="/feeds/13/entry/"]';

    bookmarkFeed1(): void{
        cy.get(this.bookmarkIcon).click({force: true});
    }
    goToBookmarked(): void{
        cy.get(this.bookmarkedTabLocator).click();
    }
    goToFeedEntry13(): void{
        cy.get(this.feedEntry288Locator).click();
    }
}